import os

from werkzeug.utils import secure_filename
from flask import Flask, request
from flask_restx import Api, Resource
from application.database.models import Base
from application.services.tweet_service import TweetService, get_author_id, get_current_user_id, get_user_by_id
from diploma.application.api import tweet_data_model, tweet_response_model

UPLOAD_FOLDER = "/python_advanced_diploma/static/images"
ALLOWED_EXTENSIONS = ["gif", "jpg", "jpeg", "png"]

app = Flask(__name__)
api = Api(app, version="1.0", title="Clone Twitter Service API", description="API for microblogging service")
api.models['TweetData'] = tweet_data_model
api.models['TweetResponse'] = tweet_response_model

tweet_service = TweetService(Base)


@api.route("/api/tweets")
class TweetResource(Resource):
    @api.expect(tweet_data_model, validate=True)
    @api.response(200, "Success", tweet_response_model)
    @api.doc(description="Create a new tweet")
    def post(self):
        """
        Create a new tweet
        """
        tweet_data = request.json
        author_id = get_author_id()
        tweet_id = tweet_service.create_tweet(tweet_data, author_id)
        response_data = {
            "result": True,
            "tweet_id": tweet_id,
        }
        return response_data, 200


@api.route("/api/medias")
class MediaUploadResource(Resource):
    @api.doc(description="Load media files")
    def post(self):
        """
        Load media files
        """
        if "file" not in request.files:
            return {"result": False, "message": "No file provided"}, 400

        file = request.files["file"]

        if (file.filename == "" or '.' not in file.filename or
                file.filename.split('.')[-1].lower() not in ALLOWED_EXTENSIONS):
            return {"result": False, "message": "Invalid file format"}, 400

        filename = secure_filename(file.filename)
        file_path = os.path.join(app.config["UPLOAD_FOLDER"], filename)
        file.save(file_path)

        return {"result": True, "message": "File uploaded successfully", "file_path": file_path}, 200


@api.route("/api/tweets/<int:id>")
class TweetIdResource(Resource):
    @api.response(200, "Success")
    @api.doc(description="Delete a tweet")
    def delete(self, id):
        """
        Delete a tweet
        """
        author_id = get_current_user_id()
        tweet_service.delete_tweet(id, author_id)
        return {"result": True}, 200


@api.route("/api/tweets/<int:id>/likes")
class LikeTweetResource(Resource):
    @api.response(200, "Success")
    @api.doc(description="Like a tweet")
    def post(self, id):
        """
        Like a tweet
        """
        user_id = get_current_user_id()
        if user_id is None:
            return {"error": "User is not authorized"}, 401

        tweet_service.like_tweet(id, user_id)

        return {"result": True}, 200

    @api.response(200, "Success")
    @api.doc(description="Unlike a tweet")
    def delete(self, id):
        """
        Unlike a tweet
        """
        user_id = get_current_user_id()

        if user_id is None:
            return {"error": "User is not authorized"}, 401

        tweet_service.unlike_tweet(id, user_id)
        return {"result": True}, 200


@api.route("/api/users/<int:id>/follow")
class FollowUserResource(Resource):
    @api.response(200, "Success")
    @api.doc(description="Follow a user")
    def post(self, id):
        """
        Follow a user
        """
        follower_id = get_current_user_id()
        tweet_service.follow_user(id, follower_id)
        return {"result": True}, 200

    @api.response(200, "Success")
    @api.doc(description="Unfollow a user")
    def delete(self, id):
        """
        Unfollow a user
        """
        follower_id = get_current_user_id()
        tweet_service.unfollow_user(id, follower_id)
        return {"result": True}, 200


@api.route("/api/timeline")
class TimelineResource(Resource):
    @api.response(200, "Success")
    @api.doc(description="Get timeline")
    def get(self):
        """
        Get timeline
        """
        user_id = get_current_user_id()
        timeline = tweet_service.get_timeline(user_id)
        return {"result": True, "tweets": timeline}, 200


@api.route("/api/users/me")
class CurrentUserResource(Resource):
    @api.response(200, "Success")
    @api.doc(description="Get current user's profile")
    def get(self):
        """
        Get current user's profile
        """
        user_id = get_current_user_id()
        user_profile = tweet_service.get_current_user_profile(user_id)
        return {"result": True, "user": user_profile}, 200


@api.route("/api/users/<int:id>")
class UserProfileResource(Resource):
    @api.response(200, "Success")
    @api.doc(description="Get user profile by ID")
    def get(self, user_id):
        """
        Get user profile by ID
        """
        user = get_user_by_id(user_id)
        if user:
            return {"result": True, "user": {
                "id": user.id,
                "name": user.name}}, 200
        else:
            return {"result": False, "message": "User not found"}, 404

