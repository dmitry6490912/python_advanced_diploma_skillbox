import logging
from diploma.application.api import app
from application.database.models import Base, engine

logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s")

logger = logging.getLogger(__name__)


if __name__ == "__main__":
    Base.metadata.create_all(engine)
    logger.info("Запуск приложения Clone Twitter...")
    app.run(debug=True, host="0.0.0.0", port=5000)
