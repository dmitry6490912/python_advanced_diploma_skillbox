import os

import pytest
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.future import select
from application.database.models import User
from dotenv import load_dotenv

load_dotenv()

DB_USER = os.getenv("DB_USER")
DB_PASSWORD = os.getenv("DB_PASSWORD")
DB_HOST = os.getenv("DB_HOST")
DB_NAME = os.getenv("DB_NAME")

DATABASE_URL = f"postgresql+asyncpg://{DB_USER}:{DB_PASSWORD}@{DB_HOST}/{DB_NAME}"
engine = create_async_engine(DATABASE_URL, echo=True)

async_session = sessionmaker(
    engine,
    expire_on_commit=False,
    class_=AsyncSession,
)

Base = declarative_base()


@pytest.fixture
async def database():
    async with async_session() as session:
        yield session


@pytest.mark.asyncio
async def test_get_all_users(database):
    async for session in database:
        await session.execute(User.__table__.delete())

        users_to_create = [
            User(name="User_1"),
            User(name="User_2"),
            User(name="User_3")
        ]
        session.add_all(users_to_create)
        await session.commit()

        result = await session.execute(select(User))
        users = result.scalars().all()

        assert len(users) == len(users_to_create)

        for user in users_to_create:
            assert user in users


@pytest.mark.asyncio
async def test_update_user(database):
    async for session in database:
        user = User(name="Test User")
        session.add(user)
        await session.commit()

        assert user.id is not None

        user.name = "Updated Test User"
        await session.commit()

        updated_user = await session.get(User, user.id)
        assert updated_user.name == "Updated Test User"


@pytest.mark.asyncio
async def test_create_user(database):
    async for session in database:
        user = User(name="Test User")
        session.add(user)
        await session.commit()

        assert user.id is not None

        result = await session.execute(select(User))
        users = result.scalars().all()
        assert user in users


@pytest.mark.asyncio
async def test_delete_user(database):
    async def delete_user():
        async with async_session() as session:
            user = User(name="Test User")
            session.add(user)
            await session.commit()

            assert user.id is not None

            user_id = user.id

            session.delete(user)
            await session.commit()

            session.expunge(user)

            deleted_user = await session.get(User, user_id)
            assert deleted_user is None

            await delete_user()
