import unittest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from application.database.models import Base, User, Tweet, Media
from tests.factories import UserFactory, TweetFactory, MediaFactory


class TestModels(unittest.TestCase):
    def setUp(self):
        self.engine = create_engine("sqlite:///:memory:")
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
        Base.metadata.create_all(self.engine)

    def tearDown(self):
        self.session.close_all()
        Base.metadata.drop_all(self.engine)

    def test_create_user(self):
        user = UserFactory.create()
        self.session.add(user)
        self.session.commit()

        self.assertIsNotNone(user.id)
        self.assertEqual(self.session.query(User).count(), 1)

    def test_create_tweet(self):
        user = UserFactory.create()
        tweet = TweetFactory.create(author=user)
        self.session.add(tweet)
        self.session.commit()

        self.assertIsNotNone(tweet.id)
        self.assertEqual(self.session.query(Tweet).count(), 1)

    def test_create_media(self):
        media = MediaFactory.create()
        self.session.add(media)
        self.session.commit()

        self.assertIsNotNone(media.id)
        self.assertEqual(self.session.query(Media).count(), 1)


if __name__ == "__main__":
    unittest.main()
