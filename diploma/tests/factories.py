import factory
from application.database.models import User, Media, Tweet


class UserFactory(factory.Factory):
    class Meta:
        model = User

    name = factory.Sequence(lambda a: f"user {a}")


class MediaFactory(factory.Factory):
    class Meta:
        model = Media

    file_src = factory.Faker("file_path")
    file_name = factory.Sequence(lambda a: f"media {a}.jpg")


class TweetFactory(factory.Factory):
    class Meta:
        model = Tweet

    content = factory.Faker("text")
    author = factory.SubFactory(UserFactory)

