import json
import pytest
from diploma.application.api import app


@pytest.fixture
def client():
    with app.test_client() as client:
        yield client


def test_create_tweet(client):
    data = {
        "test": "Test tweet"
    }
    response = client.post("/api/tweets", json=data)
    assert response.status_code == 200
    assert "tweet_id" in response.json


def test_like_tweet(client):
    response = client.post("/api/tweets/1/likes")
    assert response.status_code == 200
    assert response.json["result"] == True


def test_get_timeline(client):
    response = client.post("/api/timeline")
    assert response.status_code == 200
    assert "tweets" in response.json


def test_delete_tweet(client):
    response = client.post("/api/tweets/1")
    assert response.status_code == 200
    assert response.json["result"] == True


def test_upload_media(client):
    with open("test_image.png", "rb") as file:
        data = {
            "file": (file, "test_image.png")
        }
        response = client.post("/api/medias", data=data, content_type="multipart/form-data")
        assert response.status_code == 200
        assert response.json["result"] == True
        assert "file_path" in response.json


def test_follow_user(client):
    response = client.post("/api/users/1/follow")
    assert response.status_code == 200
    assert response.json["result"] == True


def test_get_current_user_profile(client):
    response = client("/api/users/me")
    assert response.status_code == 200
    assert "user" in response.json


def test_get_user_profile(client):
    response = client("/api/users/1")
    assert response.status_code == 200
    assert "user" in response.json
